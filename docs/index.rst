.. Omnileads Installation documentation master file, created by
   sphinx-quickstart on Wed Apr 30 10:52:05 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

*********************************
OMniLeads - Webphone Client addon
*********************************

.. image:: images/wp_webphone.png
  :width: 600px
  :align: center
  :height: 400px

En este repositorio encontrará los archivos requeridos para instalar el addon **WebPhone Client** de `OMniLeads <https://gitlab.com/omnileads/ominicontacto>`_. Este es un código que puede embeber en su página web, para permitir que sus clientes llamen a su negocio **solo con un click**, sin otra herramienta mas que el **web browser**.

Requisitos
***********

1. Una instancia de OMniLeads

  - **release-1.3.2** minimamente instalado para el **release-1.0.0** del webphone client.
  - **release-1.5.0** minimamente instalado para el **release-2.0.0** del webphone client.
  - **release-1.7.0** minimamente instalado para el **release-3.0.0** del webphone client.
  - **release-1.12.0** minimamente instalado para el **release-3.5.0** del webphone client.

.. important::

	Esta instancia **debe ser** accesible desde Internet y debe trabajar con **certificados confiables**. Puede observar la documentación de OMnileads que habla sobre como instalar un servidor en la nube: `OMniLeads Cloud <https://documentacion-omnileads.readthedocs.io/es/stable/install.html#omnileads-cloud>`_.

2. La pagina web donde va a embeber el webphone tiene que estar usando HTTPS (Requisito de WebRTC).

.. toctree::
   :hidden:

   oml_configuration.rst
   webpage_configuration.rst
   wordpress.rst
   webphone_demo.rst
   changelog.rst

.. note::

  A partir de la versión 3.5.0 existe la posibilidad de hacer Videollamada.
