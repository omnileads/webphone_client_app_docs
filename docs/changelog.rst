.. _about_webphone_design:

**********
Changelog
**********

**Release 1.0.0:**
  - Initial release, possibility of edit, create and remove webphone client users
**Release 2.0.0:**
  - Migrated the code to python3 and django 2.2.7
**Release 3.0.0:**
  - Added compatibility of addon with OMniLeads 1.7.0
**Release 3.5.0:**
  - Added the possibility to make video calls from the webphone.
