**************************
Configuración en OMniLeads
**************************

Deje la configuración de OMniLeads en nuestras manos
*****************************************************

Usted no necesita tener su propia instancia de OMniLeads para que funcione su cliente Webphone, le podemos proveer de un usuario y contraseña para conectarse a nuestro OMniLeads hosteado en la nube, 24/7. Para mas información puede `contactarnos <https://www.freetechsolutions.com.ar/contacto/>`_.

**En la página de contacto puede encontrar el WebphoneClient funcionando!!!**

**Si decide usar nuestro OMniLeads hosteado, puede ir directamente a la seccion** :ref:`about_embebing_webphone`

Pasos para instalar el módulo WebphoneClient en OMniLeads
**********************************************************

1. Ejecutar el script install.sh contenido en la carpeta que descargó.
2. Reiniciar el servicio OMniLeads para tener el nuevo código:

.. code-block:: bash

  ./install.sh
  service omnileads restart

3. Logueese con su cuenta de admin y vaya a la página de admin https://OML_HOST/admin/. Vaya a la sección "Constance" y clickee en "Config"

.. image:: images/django_admin.png

4. Setear la variable *WEBPHONE_CLIENT_ENABLED* a True, clickeando en el box. Esto habilitará el módulo.
5. Setear la variable *WEBPHONE_CLIENT_TTL*. El tiempo de vida de las credenciales del webphonees de 1200 segundos (20 minutos)
6. Setear la variable *WEBPHONE_VIDEO_DOMAIN* con el servidor de jitsi a utilizar en caso de que quiera permitir llamadas de video.

.. image:: images/constance.png

Luego hacer click en "Save"

Registro de Key OMniLeads
**************************

Es condición necesaria que la instancia esté registrada, acción que se efectúa desde el Menú
Ayuda>Registrar del panel de Administración:

.. image:: images/registro-oml.png
        :align: center

Webphone Client Pro está disponible para versiones de OmniLeads 1.3.2 o superiores. Si tiene dudas
en cómo obtener la key de activación, contacte a info@omnileads.net.

Configurando el WebPhone client
*******************************

Con la variable WEBPHONE_CLIENT_ENABLED seteada en True, ahora puede crear un usuario WebphoneClient, logueandose a OMniLeads como admin:

1. Ir a Usuarios y Grupos --> Nuevo usuario. Escoger el rol "Webphone Client" para este usuario.

.. image:: images/client_user.png

2. Puede ver el usuario creado en Usuarios y Grupos --> WebphoneClients

.. image:: images/webphone_client.png

.. note::

  Recuerde el usuario y contraseña de este usuario. Los va a necesitar para para poder obtener las credenciales SIP de login usando la API de OMniLeads.

Acerca del Licenciamiento
**************************

Webphone Client es un módulo comercial, motivo por el cual su funcionamiento está sujeto a la activación de producto a partir de la vinculación de una key (llave de acceso) con el Servidor de Llaves.

El esquema de licenciamiento es de carácter permanente (key definitiva), y se brinda 1 año de actualizaciones para cobertura de bug-fixing y nuevas features sobre el release instalado. En caso de precisar upgrades de releases superado el año de la compra, se puede actualizar la key a un costo módico.

La key permite activar el módulo en 1 (una) sola instancia a la vez, y no está sujeta a la cantidad de agentes operando en la plataforma.

Si tiene dudas en cómo obtener la key de activación, contacte a info@omnileads.net.
