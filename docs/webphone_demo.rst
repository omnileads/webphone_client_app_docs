**************
WebPhone Demo
**************

Este es una demo de como funciona el webphone, utilizando Flask como backend. Todo el contenido esta en la carpeta `demo`. Para ejecutar este código ud debe:

1. Editar el objeto config en el archivo `views.py` con la configuración de su OMniLeads

.. code-block:: python2

  config = {
       'URL_API_CREDENTIALS': 'https://OML_HOST/api/v1/webphone/credentials/', // Your OML instance
       'client_username': 'client_username',   // Your OML WebPhone Client User username.
       'client_password': 'clientpassword',    // Your OML WebPhone Client User password.
  }

2. Editar estas variables en el archivo `templates/index.html`

.. code-block:: bash

  'KamailioHost': "X.X.X.X", // This is the LAN IP of you OMniLeads instance
  'WebSocketPort': "443", // This is the port you se to connect via Web browser to your OMniLeads instance
  'WebSocketHost': "domain.example.com", // This is the domain name you use to connect via Web browser to your OMniLeads instance

3. Instalar flask y correrlo localmente

.. code-block:: bash

    $ pip install Flask
    $ pip install pyopenssl
    $ cd demo/
    $ FLASK_APP=webphone.py FLASK_DEBUG=1 flask run --cert=adhoc

Por ultimo, navegar a:
    https://localhost:5000/
