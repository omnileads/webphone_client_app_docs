.. _about_wordpress_plugin:

*****************
WordPress plugin
*****************

Creamos un sencillo plugin de WordPress, así usted puede insertar el WebPhone client como un Widget, en cualquier parte de su página web. El plugin aun no ha sido publicado en el repositorio de wordpress, por lo que tiene que ser instalado manualmente:

Installation
************

1. En la página de administración de Wordpress ir a Plugin -> Añadir nuevo y subir el archivo *oml-webphone-client.zip*.
2. Click en el botón "Instalar" y esperar a que instale.
3. Una vez instalado Activar el plugin en la lista de plugins.

Configuración
*************

1. Ir a Settings -> OMniLeads WebphoneClient, allí va a configurar su webphone client con las credenciales del Usuario WebPhone Cliente que usted creó en su instancia OMniLeads. Luego ingrese la URL de la API.

.. image:: images/wp_admin_page.png

2. Una vez configurado esto, ir al menu Widget, donde verá que disponible el "OML WebphoneClient Widget". Aquí se van a configurar mas cosas:

.. image:: images/wp_widget_page.png

* **Titulo:** un titulo para su webphone
* **KamailioHost:** la IP LAN de su instancia de OMniLeads. **Si nosotros estamos hosteando el OMniLeads preguntenos por este parámetro**
* **Opciones de estilo:** el nombre de los IDs y clases para crear un diseño custom del webphone usando CSS. Observe la seccion :ref:`about_webphone_design`
* **Botones de llamada:** puede insertar un máximo de cuatro botones de llamada. Acá configura el número a llamar y el nombre que va a tener el botón.

3. Click en guardar para salvar la configuración y ya puede observar en su página el widget insertado.

Actualización de plugin
***********************

Para actualizar el plugin es necesario primero:

1. En la página de administración de Wordpress ir a Plugin encontrar el plugin OMniLeads Webphone Addon
2. Desactivar y eliminar el plugin
3. Volver a instalarlo con el .zip actualizado.

.. note::

  Es necesario volver a configurar los datos en Settings -> OMniLeads WebphoneClient, pues el plugin se ha desinstalado y vuelto a instalar.
